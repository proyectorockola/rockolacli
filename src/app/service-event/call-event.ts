import { Injectable } from '@angular/core';
import { EventEmitter, Output} from '@angular/core';

@Injectable()
export class CallEvent {
  //idArtista: number;

  @Output() change: EventEmitter<number> = new EventEmitter();
  @Output() select: EventEmitter<number> = new EventEmitter();
  @Output() select2: EventEmitter<number> = new EventEmitter();


  eventoDesdeArtista(id){
    //this.idArtista = this.idArtista;
    this.change.emit(id);
  }

  eventSelect(key){
    this.select.emit(key);
  }

  eventSelect2(key){
    this.select2.emit(key);
  }

}
