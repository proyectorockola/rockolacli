import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../local-storage/local-storage-service';
import { Song } from './song';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  songs = new Song()
  constructor(private localStorageService: LocalStorageService) { }

  ngOnInit() {

    let temp = this.localStorageService.getValueFromKey('cola-canciones');
    if (temp) {
      this.songs = temp
    }
  }

}
