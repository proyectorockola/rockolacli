import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';



@Injectable()
export class LocalStorageService {

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }



  public getValueFromKey(key: string){
    return this.storage.get(key);
  }

  public setValueKey(value: any, key: string){
      this.storage.set(key, value)
  }



}
