import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Song } from './song';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SongsService {

private urlEndPoint: string = 'http://localhost:8080/api/songs';
  constructor(private http: HttpClient) { }

  getSongs(): Observable<Song[]>{
    return this.http.get<Song[]>(this.urlEndPoint);
  }
}
