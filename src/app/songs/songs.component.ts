import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Song } from './song';
import { SongsService } from './songs.service';
import { HostBinding } from '@angular/core'
import { CallEvent } from '../service-event/call-event';
import { MzModalService } from 'ngx-materialize';
import { NoCreditAlertComponent } from '../alert/no-credit-alert.component';
import { LocalStorageService } from '../local-storage/local-storage-service';
import { MzModalComponent, MzBaseModal } from 'ngx-materialize';
import { ArtistasService } from '../artistas/artistas.service';
import { Artist } from './artist';

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css']
})
export class SongsComponent implements OnInit {
  @ViewChild('fixedFooterModal', { static: false }) modal: MzModalComponent;
  @ViewChild('noCreditModal', { static: false }) noCreditModal: MzModalComponent;
  @ViewChild('panel', { static: false })  panel:ElementRef

  @HostBinding('class.is-open')

  idArtista: number;
  position = 0;
  credit = 0;
  interval
  item: Song;
  artist = new Artist();
  songs: Song[];
  colaSongs: Song[] = [];
  constructor(private songsService: SongsService,
    private callEvent: CallEvent,
    private modalService: MzModalService,
    private localStorageService: LocalStorageService,
    private artistasService: ArtistasService) { }

  ngOnInit() {
    let credit = this.localStorageService.getValueFromKey('credito');
    if (credit) {
      this.credit = credit
    }


    this.callEvent.change.subscribe(artista => {
        this.artist =  artista as Artist

      this.llamarApiSong();
    })

    this.callEvent.select2.subscribe(key => {
      if(key == 'U'){
        this.DownSong()
        this.onNextSearchPosition()
        this.panelDownSong()

      }else{
        this.sendUpSong()
        this.onPreviousSearchPosition()
        this.panelUpSong()
      }
    })

    //como este recibe utilizas este subscribe de arriba

    let temp = this.localStorageService.getValueFromKey('cola-canciones')
    if (temp) {
      this.colaSongs = temp; // metodo que asigna si viene un valor en credito
    }

  }

  public onPreviousSearchPosition(): void {
    this.panel.nativeElement.scrollTop -= 35;
  }
//llama a este cuando baje
  public onNextSearchPosition(): void {
   this.panel.nativeElement.scrollTop += 35;
 }

panelDownSong(){
  if(this.position == 0){
    this.panel.nativeElement.scrollTop = 0
  }
}

panelUpSong(){
  if(this.position == (this.songs.length-1)){
    this.panel.nativeElement.scrollTop += (this.position * 35)
  }
}

  llamarApiSong() {
    this.songsService.getSongs().subscribe(val => this.songs = val);
  }
  //this.songsService.getSongs().subscribe(val => this.songs = val );
  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    ready: (modal, trigger) => { // Callback for Modal open. Modal and trigger parameters available.

      console.log(modal, trigger);
    },
    complete: () => {
      this.recibirClose();

    } // Callback for Modal close

  };

  recibirClose() {
    console.log('se cerro')
  }

  close() {
    this.modal.closeModal();
  }

  customOpenModal(item: Song) { //metodo del html
    this.item = item;
    if (!this.credit || this.credit == 0) {
      this.noCreditModal.openModal();
      this.startClosetimerModal();
    } else {
      this.modal.openModal();
    }
  }

  startClosetimerModal() {
    this.interval = setInterval(() => {
      this.noCreditModal.closeModal();
      this.cleanTimer();
    }, 3000)
  }

  cleanTimer() {
    clearInterval(this.interval);
  }

  closeAndAgree() {
    console.log(this.item)
    this.colaSongs.push(this.item);
    console.log(this.colaSongs)
    this.credit--
    this.localStorageService.setValueKey(this.credit, 'credito')
    this.modal.closeModal();
    this.localStorageService.setValueKey(this.colaSongs, 'cola-canciones');
  }

  getSongAdded() {
    if (this.colaSongs && this.colaSongs.length > 0) {
      var cancionActual = this.colaSongs[0];
    }

  }

  DownSong(){
  if(this.position == (this.songs.length-1)){
    this.position = 0;
  }else{
    if(this.songs.length)
    this.position ++;
  }
  }

  sendUpSong(){
    if(this.position == 0){
      this.position = (this.songs.length-1);
    }else{
      if(this.songs.length)
      this.position --;
    }
  }

}
