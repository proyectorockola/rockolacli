import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ArtistasComponent } from './artistas/artistas.component';
import { MzButtonModule } from 'ngx-materialize';
import { MzCardModule } from 'ngx-materialize';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MzSidenavModule } from 'ngx-materialize';
import { MzInputModule } from 'ngx-materialize';
import { MzNavbarModule } from 'ngx-materialize'
import { MzIconModule, MzIconMdiModule } from 'ngx-materialize';
import { MzCollectionModule } from 'ngx-materialize'
import { SongsComponent } from './songs/songs.component';
import { ArtistasService } from './artistas/artistas.service';
import { SongsService } from './songs/songs.service';
import { HttpClientModule } from '@angular/common/http';
import { CallEvent } from './service-event/call-event';
import { VideoComponent } from './video/video.component';
import { MzModalModule } from 'ngx-materialize';
import { AlertComponent } from './alert/alert.component';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { LocalStorageService } from './local-storage/local-storage-service';
import { NoCreditAlertComponent } from './alert/no-credit-alert.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [
    AppComponent,
    ArtistasComponent,
    SongsComponent,
    VideoComponent,
    AlertComponent,
    NoCreditAlertComponent,

  ],
  imports: [
    BrowserModule,
    MzButtonModule,
    MzCardModule,
    BrowserAnimationsModule,
    MzSidenavModule,
    MzIconModule,
    MzIconMdiModule,
    HttpClientModule,
    MzNavbarModule,
    MzCollectionModule,
    MzModalModule,
    StorageServiceModule,
    MzInputModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [ArtistasService, SongsService, CallEvent, LocalStorageService],
  bootstrap: [AppComponent],
  entryComponents:[AlertComponent, NoCreditAlertComponent]
})
export class AppModule { }
