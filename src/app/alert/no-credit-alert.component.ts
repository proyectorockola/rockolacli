import { Component } from '@angular/core';
import { MzBaseModal } from 'ngx-materialize';

@Component({
  selector: 'app-no-credit-alert',
  templateUrl: './no-credit-alert.component.html',
  styleUrls: ['./no-credit-alert.component.css']
})
export class NoCreditAlertComponent extends MzBaseModal {

}
