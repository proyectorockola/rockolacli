import { Component} from '@angular/core';
import { MzBaseModal } from 'ngx-materialize';
import { FormGroup, FormBuilder,Validators } from '@angular/forms'
import { LocalStorageService } from '../local-storage/local-storage-service';
import { Lyric } from './lyric'

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})

export class AlertComponent extends MzBaseModal{

  //recibeLyric
  lyric = new Lyric();
  configKeyboard: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private localStorageService: LocalStorageService){
    super();

    let  recibeLyric = this.localStorageService.getValueFromKey('ingreso_teclas')
    if (recibeLyric){
      this.lyric = recibeLyric;
    }
    this.createConfigkeyboard();

  }
  //en este caso podemos hacer esto

  createConfigkeyboard(){
    this.configKeyboard = this.formBuilder.group({
      subirCanciones: [this.lyric.subir_Canciones,[Validators.required,Validators.maxLength(1)]],
      bajarCanciones: [this.lyric.bajar_Canciones],
      irDerecha: [this.lyric.ir_Derecha],
      irIzquierda: [this.lyric.ir_Izquierda],
      señalMonedero: [this.lyric.señal_Monedero],
      pasarAlSiguienteTema: [''],
      apagarComputadora: [''],
      subirVolumen: [''],
      bajarVolumen: [''],
      reproducirCancion: ['']

    })
  }

  saveKeyboard(){
    this.lyric.bajar_Canciones = this.configKeyboard.controls.bajarCanciones.value
    this.lyric.subir_Canciones = this.configKeyboard.controls.subirCanciones.value
    this.lyric.ir_Derecha = this.configKeyboard.controls.irDerecha.value
    this.lyric.ir_Izquierda = this.configKeyboard.controls.irIzquierda.value
    this.lyric.señal_Monedero = this.configKeyboard.controls.señalMonedero.value
    this.localStorageService.setValueKey(this.lyric, 'ingreso_teclas');
  }

  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 300, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    ready: (modal, trigger) => { // Callback for Modal open. Modal and trigger parameters available.

      console.log(modal, trigger);
    },
    complete: () => {  } // Callback for Modal close
  };







}
