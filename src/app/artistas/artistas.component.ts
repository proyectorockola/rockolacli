import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Artistas } from './artistas';
import { ArtistasService } from './artistas.service';
import { CallEvent } from '../service-event/call-event';


@Component({
  selector: 'app-artistas',
  templateUrl: './artistas.component.html',
  styleUrls: ['./artistas.component.css']
})
export class ArtistasComponent implements OnInit {
  @ViewChild('panel', { static: false }) public panel:ElementRef<any>;

  artistas: Artistas[]; //arreglo artists vacio o cero
  position = 0; //para index en html
  constructor(private artistasService: ArtistasService,
    private callEvent: CallEvent) {}


  ngOnInit() {

    this.artistasService.getArtistas().subscribe(val => this.artistas = val);
    //console.log(val)
    this.callEvent.select.subscribe(key => {
      if(key == 'D'){
        this.downArtist();
        this.onNextSearchPosition();
        this.panelDownArtist()
      }else{
        this.upArtist();
        this.onPreviousSearchPosition();
        this.panelUpArtists()
      }
      //aqui va a llegar cuando apresiones una tecla ya derecha izquierda que es igual subir y barjar artistas
    })
}
  //  usuario --> click -->  artista recibe click --->  el intermediario recibe el click ---> se lo pasa a songs --->
  // y songs recibe el evento y llama al api songs//
//llama este cuando suba//
click(artista) {
  this.callEvent.eventoDesdeArtista(artista);
}

  public onPreviousSearchPosition(): void {
    this.panel.nativeElement.scrollTop -= 140;
  }
//llama a este cuando baje
  public onNextSearchPosition(): void {
   this.panel.nativeElement.scrollTop += 140;
 }

  panelDownArtist(){
    if(this.position == 0){
      this.panel.nativeElement.scrollTop = 0;
    }
  }

  panelUpArtists(){
    if(this.position == (this.artistas.length-1)){
      this.panel.nativeElement.scrollTop += (this.position * 140)
    }
  }

  downArtist() {
    if (this.position == (this.artistas.length - 1)) {
      this.position = 0
      console.log(this.position, 'a')
    } else {
      if (this.artistas.length) {
        this.position++
        console.log(this.position, 'b')
      }
    }
    this.click(this.artistas[this.position])
  }

  upArtist() {
    if (this.position == 0) {
      this.position = (this.artistas.length - 1);
    } else {
      if (this.artistas) {
        this.position--
      }
    }
    this.click(this.artistas[this.position])
  }
}
