import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Artistas } from './artistas';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArtistasService {

  private urlEndPoint: string = 'http://localhost:8080/api/artists';
  constructor(private http: HttpClient) { }

  getArtistas(): Observable<Artistas[]>{
    return this.http.get<Artistas[]>(this.urlEndPoint);
  }
}
