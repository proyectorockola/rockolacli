import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MzModalService } from 'ngx-materialize';
import { LocalStorageService } from './local-storage/local-storage-service';
import { MzSidenavComponent } from 'ngx-materialize';
import { AlertComponent } from './alert/alert.component';
import { CallEvent } from './service-event/call-event';
import { Lyric } from './alert/lyric';

export enum KEY_CODE {
  RIGHT_ARROW = 39,
  LEFT_ARROW = 37,
  UP_ARROW = 38,
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  lyric = new Lyric();
  value = 0;
  title = 'rockolacli';
  artistas = [];//ubuieras tenido que hacer la logica de artistas aqui//
  //como te dije esta es tu placa ella no sabe que es lo que hace el componente ARTISTAS
  //lo que si  es que te vas a poder comunicar con artistas pero solo tendras salias y entradas
  //no logica
  constructor(private localStorageService: LocalStorageService,
              private modalService: MzModalService,
              private callEvent: CallEvent) { }
  @ViewChild('sidenav', { static: false}) myNav: MzSidenavComponent;

  ngOnInit(): void {

      let temp = this.localStorageService.getValueFromKey('credito')
    if (temp) {
      this.value = temp; // metodo que asigna si viene un valor en credito
    }

  let  recibeLyric = this.localStorageService.getValueFromKey('ingreso_teclas')
  if (recibeLyric){
    this.lyric = recibeLyric;
  }

    //puede ser segunda opcion que asigna so no viene un valor en credito
    /*this.value = this.localStorageService.getValueFromKey('credito');
    if(!this.value){
      this.value = 0;
    }*/

  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    console.log(event);

    if (event.key == this.lyric.subir_Canciones.toLowerCase()) {
      this.sendUpSong();
    }

    if (event.key == this.lyric.bajar_Canciones.toLowerCase()) {
      this.sendDownSong();
    }

    if (event.key == this.lyric.ir_Derecha.toLowerCase()) {
      this.sendUpArtist(); //ir derecha = subir artistas
    }

    if (event.key == this.lyric.ir_Izquierda.toLowerCase()) {
      this.sendDownArtist(); // ir equierda = a bajar artistas
    }

    if (event.key == this.lyric.señal_Monedero.toLowerCase()) {

    }

    if (event.keyCode === KEY_CODE.RIGHT_ARROW) {
      this.creditoIncrement();
    }

    if (event.keyCode === KEY_CODE.LEFT_ARROW) {
      this.creditoDecrement();
    }

    if (event.keyCode === KEY_CODE.UP_ARROW) {
      this.openMenu()
    }
  }

  sendUpSong() {
    this.callEvent.eventSelect2('U');
  }

  sendDownSong(){
    this.callEvent.eventSelect2('D');
  }

  sendDownArtist() {
    this.callEvent.eventSelect('D');
  }

  sendUpArtist(){
    this.callEvent.eventSelect('U');
  }

  openMenu(){
    this.myNav.opened = true;
  }

  public openServiceModal() {
  this.modalService.open(AlertComponent);
}

  creditoIncrement() {
    this.value++;
    this.localStorageService.setValueKey(this.value, 'credito')
  }

  creditoDecrement() {
    if (this.value >= 1) {
      this.value--;
    } else {
      console.log('sin creditos');
    }

  }

}
